# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2]

### Added
- Support for arm64.

### Fixed
- LINUX_LINKER_LIBS now being used when compiling for Linux.
- WINDOWS_LINKER_LIBS now being used when compiling for Windows.

## [1.0.1]

### Added
- Unlicense License

## [1.0.0]
### Added
- Universal C Makefile
- Project File Structure
- README to help understand the usage of the makefile
- This CHANGELOG to track future changes to the makefile
